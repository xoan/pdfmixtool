<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>О программе PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="43"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="53"/>
        <source>Version %1</source>
        <translation>Версия %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="65"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>Программа для разбиения, объединения, поворота и компоновки PDF файлов.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="66"/>
        <source>Website</source>
        <translation>Вебсайт</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="73"/>
        <source>About</source>
        <translation>Про</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="89"/>
        <source>Authors</source>
        <translation>Авторы</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="91"/>
        <source>Translators</source>
        <translation>Переводчики</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="100"/>
        <source>Credits</source>
        <translation>Пожертвования</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="118"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="128"/>
        <source>Submit a pull request</source>
        <translation>Отправить запрос о помощи</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>Report a bug</source>
        <translation>Сообщение об ошибке</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="130"/>
        <source>Help translating</source>
        <translation>Помочь с переводом</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="137"/>
        <source>Contribute</source>
        <translation>Пожертвовать</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="153"/>
        <source>Changelog</source>
        <translation>Журнал изменений</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Редактировать многостраничный профиль</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="60"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="158"/>
        <source>Left</source>
        <translation>Слева</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="61"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <source>Center</source>
        <translation>По центру</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="62"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="160"/>
        <source>Right</source>
        <translation>Справа</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="162"/>
        <source>Top</source>
        <translation>Сверху</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="164"/>
        <source>Bottom</source>
        <translation>Снизу</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="97"/>
        <source>Name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="105"/>
        <source>Output page size</source>
        <translation>Размер выходящей страницы</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="107"/>
        <source>Standard size:</source>
        <translation>Стандартный размер:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="110"/>
        <source>Custom size:</source>
        <translation>Собственный размер:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="113"/>
        <source>Width:</source>
        <translation>Ширина:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="116"/>
        <source>Height:</source>
        <translation>Высота:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="124"/>
        <source>Pages layout</source>
        <translation>Макет страницы</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="126"/>
        <source>Rows:</source>
        <translation>Строки:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Columns:</source>
        <translation>Столбцы:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rotation:</source>
        <translation>Вращение:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Spacing:</source>
        <translation>Интервал:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="143"/>
        <source>Pages alignment</source>
        <translation>Выравнивание страниц</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="145"/>
        <source>Horizontal:</source>
        <translation>Горизонтально:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="148"/>
        <source>Vertical:</source>
        <translation>Вертикально:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="156"/>
        <source>Margins</source>
        <translation>Поля</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="33"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>Изменить свойства PDF-файлов</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="36"/>
        <source>No rotation</source>
        <translation>Без вращения</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="41"/>
        <source>Disabled</source>
        <translation>Отключено</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="87"/>
        <source>OK</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="100"/>
        <source>Multipage:</source>
        <translation>Многостраничный:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="102"/>
        <source>Rotation:</source>
        <translation>Вращение:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="97"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>portrait</source>
        <translation>портретный</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="97"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>landscape</source>
        <translation>альбомный</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="143"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="201"/>
        <source>All</source>
        <translation>Все</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/inputpdffiledelegate.cpp" line="98"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="221"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n страница</numerusform>
            <numerusform>%n страницы</numerusform>
            <numerusform>%n страниц</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="145"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="203"/>
        <source>Pages:</source>
        <translation>Страницы:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="146"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="204"/>
        <source>Multipage:</source>
        <translation>Многостраничный:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="149"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="207"/>
        <source>Disabled</source>
        <translation>Отключено</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="150"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="208"/>
        <source>Rotation:</source>
        <translation>Вращение:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="176"/>
        <source>Disabled</source>
        <translation>Отключено</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="189"/>
        <source>No rotation</source>
        <translation>Без вращения</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="195"/>
        <source>Pages:</source>
        <translation>Страницы:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="197"/>
        <source>Multipage:</source>
        <translation>Многостраничный:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="199"/>
        <source>Rotation:</source>
        <translation>Вращение:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="150"/>
        <source>Add PDF file</source>
        <translation>Добавить PDF-файл</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="151"/>
        <source>Move up</source>
        <translation>Сдвинуть выше</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="152"/>
        <source>Move down</source>
        <translation>Сдвинуть ниже</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="153"/>
        <source>Remove file</source>
        <translation>Удалить файл</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="168"/>
        <source>About</source>
        <translation>Про</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="172"/>
        <location filename="../src/mainwindow.cpp" line="173"/>
        <source>Generate PDF</source>
        <translation>Создать PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="110"/>
        <source>PDF Mix Tool</source>
        <translation>Соединение PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <location filename="../src/mainwindow.cpp" line="426"/>
        <source>PDF generation error</source>
        <translation>Ошибка при создании PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="210"/>
        <source>Select one or more PDF files to open</source>
        <translation>Выберите один или несколько PDF-файлов для открытия</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="143"/>
        <source>Edit</source>
        <translation>Правка</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="145"/>
        <source>Main toolbar</source>
        <translation>Главная панель инструментов</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="164"/>
        <source>Menu</source>
        <translation>Меню</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="167"/>
        <source>Multipage profiles…</source>
        <translation>Многостраничный профиль…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="169"/>
        <source>Exit</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="212"/>
        <location filename="../src/mainwindow.cpp" line="449"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF-файлы (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="349"/>
        <source>Output pages: %1</source>
        <translation>Вывод страниц: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="359"/>
        <source>You must add at least one PDF file.</source>
        <translation>Добавьте хотя бы один PDF-файл.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="382"/>
        <source>&lt;li&gt;Invalid character &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;неверный символ &quot;&lt;b&gt;%1&lt;/b&gt;&quot; в фильтре страниц файла &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="388"/>
        <source>&lt;li&gt;Invalid interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;неверный интервал &quot;&lt;b&gt;%1&lt;/b&gt;&quot; в фильтре страниц файла &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="394"/>
        <source>&lt;li&gt;Boundaries of interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; are out of allowed interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;границы интервала &quot;&lt;b&gt;%1&lt;/b&gt;&quot; в фильтре страниц файла &quot;&lt;b&gt;%2&lt;/b&gt;&quot; вышли за пределы&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="413"/>
        <source>&lt;li&gt;Interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; is overlapping with another interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;интервал &quot;&lt;b&gt;%1&lt;/b&gt;&quot;в фильтре страниц файла &quot;&lt;b&gt;%2&lt;/b&gt;&quot; пересекается с другим интервалом&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>&lt;p&gt;The PDF generation failed due to the following errors:&lt;/p&gt;</source>
        <translation>&lt;p&gt;Не удалось создать PDF из-за следующих ошибок:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="431"/>
        <source>&lt;p&gt;The following problems were encountered while generating the PDF file:&lt;/p&gt;</source>
        <translation>&lt;p&gt;При создании файла PDF возникли следующие проблемы:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="447"/>
        <source>Save PDF file</source>
        <translation>Сохранить PDF-файл</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>Новый профиль…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="32"/>
        <source>Delete profile</source>
        <translation>Удалить профиль</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="37"/>
        <source>Manage multipage profiles</source>
        <translation>Управление многостраничными профилями</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="94"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="96"/>
        <source>Custom profile</source>
        <translation>Собственный профиль</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <source>Profile name can not be empty.</source>
        <translation>Имя профиля не может быть пустым.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="147"/>
        <source>Disabled</source>
        <translation>Отключено</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Profile name already exists.</source>
        <translation>Профиль с таким именем уже существует.</translation>
    </message>
</context>
</TS>
