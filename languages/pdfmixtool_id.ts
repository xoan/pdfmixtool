<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>Tentang PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="43"/>
        <source>Close</source>
        <translation>Tutup</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="53"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="65"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>Sebuah aplikasi untuk memisahkan, menyatukan, memutar dan mencampurkan berkas PDF.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="66"/>
        <source>Website</source>
        <translation>Situs web</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="73"/>
        <source>About</source>
        <translation>Tentang</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="89"/>
        <source>Authors</source>
        <translation>Pencipta</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="91"/>
        <source>Translators</source>
        <translation>Penerjemah</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="100"/>
        <source>Credits</source>
        <translation>Kredit</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="118"/>
        <source>License</source>
        <translation>Lisensi</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="128"/>
        <source>Submit a pull request</source>
        <translation>Kirim pull request</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>Report a bug</source>
        <translation>Laporkan bug</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="130"/>
        <source>Help translating</source>
        <translation>Bantu menerjemahkan</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="137"/>
        <source>Contribute</source>
        <translation>Berkontribusi</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="153"/>
        <source>Changelog</source>
        <translation>Catatan Perubahan</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Sunting profil multi halaman</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="60"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="158"/>
        <source>Left</source>
        <translation>Kiri</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="61"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <source>Center</source>
        <translation>Tengah</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="62"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="160"/>
        <source>Right</source>
        <translation>Kanan</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="162"/>
        <source>Top</source>
        <translation>Atas</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="164"/>
        <source>Bottom</source>
        <translation>Bawah</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="97"/>
        <source>Name:</source>
        <translation>Nama:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="105"/>
        <source>Output page size</source>
        <translation>Ukuran halaman keluaran</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="107"/>
        <source>Standard size:</source>
        <translation>Ukuran Standar:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="110"/>
        <source>Custom size:</source>
        <translation>Ukuran kustom:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="113"/>
        <source>Width:</source>
        <translation>Lebar:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="116"/>
        <source>Height:</source>
        <translation>Tinggi:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="124"/>
        <source>Pages layout</source>
        <translation>Tata letak halaman</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="126"/>
        <source>Rows:</source>
        <translation>Baris:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Columns:</source>
        <translation>Kolom:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rotation:</source>
        <translation>Rotasi:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Spacing:</source>
        <translation>Jarak:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="143"/>
        <source>Pages alignment</source>
        <translation>Perataan halaman</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="145"/>
        <source>Horizontal:</source>
        <translation>Horizontal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="148"/>
        <source>Vertical:</source>
        <translation>Vertikal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="156"/>
        <source>Margins</source>
        <translation>Margin</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="33"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>Sunting properti berkas PDF</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="36"/>
        <source>No rotation</source>
        <translation>Tidak ada rotasi</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="41"/>
        <source>Disabled</source>
        <translation>Nonaktifkan</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="87"/>
        <source>OK</source>
        <translation>OKE</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="100"/>
        <source>Multipage:</source>
        <translation>Multi halaman:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="102"/>
        <source>Rotation:</source>
        <translation>Rotasi:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="97"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>portrait</source>
        <translation>portrait</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="97"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>landscape</source>
        <translation>landscape</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="143"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="201"/>
        <source>All</source>
        <translation>Semua</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/inputpdffiledelegate.cpp" line="98"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="221"/>
        <source>%n page(s)</source>
        <translation type="unfinished"><numerusform></numerusform>
        </translation></message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="145"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="203"/>
        <source>Pages:</source>
        <translation>Halaman:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="146"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="204"/>
        <source>Multipage:</source>
        <translation>Multi halaman:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="149"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="207"/>
        <source>Disabled</source>
        <translation>Nonaktifkan</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="150"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="208"/>
        <source>Rotation:</source>
        <translation>Rotasi:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="176"/>
        <source>Disabled</source>
        <translation>Nonaktifkan</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="189"/>
        <source>No rotation</source>
        <translation>Tidak ada rotasi</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="195"/>
        <source>Pages:</source>
        <translation>Halaman:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="197"/>
        <source>Multipage:</source>
        <translation>Multi halaman:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="199"/>
        <source>Rotation:</source>
        <translation>Rotasi:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="150"/>
        <source>Add PDF file</source>
        <translation>Tambahkan berkas PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="151"/>
        <source>Move up</source>
        <translation>Pindah ke atas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="152"/>
        <source>Move down</source>
        <translation>Pindah ke bawah</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="153"/>
        <source>Remove file</source>
        <translation>Hapus berkas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="168"/>
        <source>About</source>
        <translation>Tentang</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="172"/>
        <location filename="../src/mainwindow.cpp" line="173"/>
        <source>Generate PDF</source>
        <translation>Menggenerasi PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="110"/>
        <source>PDF Mix Tool</source>
        <translation>PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <location filename="../src/mainwindow.cpp" line="426"/>
        <source>PDF generation error</source>
        <translation>gagal menggenerasi PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="210"/>
        <source>Select one or more PDF files to open</source>
        <translation>Pilih satu atau lebih berkas PDF untuk dibuka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="143"/>
        <source>Edit</source>
        <translation>Sunting</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="145"/>
        <source>Main toolbar</source>
        <translation>Toolbar utama</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="164"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="167"/>
        <source>Multipage profiles…</source>
        <translation>Profil multi halaman…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="169"/>
        <source>Exit</source>
        <translation>Keluar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="212"/>
        <location filename="../src/mainwindow.cpp" line="449"/>
        <source>PDF files (*.pdf)</source>
        <translation>Berkas PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="349"/>
        <source>Output pages: %1</source>
        <translation>Halaman keluaran: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="359"/>
        <source>You must add at least one PDF file.</source>
        <translation>Anda harus menambahkan setidaknya satu berkas PDF.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="382"/>
        <source>&lt;li&gt;Invalid character &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;Karakter tidak valid &quot;&lt;b&gt;%1&lt;/b&gt;&quot; dalam filter halaman dari berkasi &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="388"/>
        <source>&lt;li&gt;Invalid interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;Interval tidak valid &quot;&lt;b&gt;%1&lt;/b&gt;&quot; dalam filter halaman dari berkas &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="394"/>
        <source>&lt;li&gt;Boundaries of interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; are out of allowed interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;Batas-batas interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; dalam filter halaman dari berkas &quot;&lt;b&gt;%2&lt;/b&gt;&quot; berada di luar interval yang diizinkan&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="413"/>
        <source>&lt;li&gt;Interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; is overlapping with another interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;Interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; dalam filter halaman dari berkas &quot;&lt;b&gt;%2&lt;/b&gt;&quot; tumpang tindih dengan interval lain&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>&lt;p&gt;The PDF generation failed due to the following errors:&lt;/p&gt;</source>
        <translation>&lt;p&gt;Gagal menggenerasi PDF karena eror berikut ini:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="431"/>
        <source>&lt;p&gt;The following problems were encountered while generating the PDF file:&lt;/p&gt;</source>
        <translation>&lt;p&gt;Masalah berikut muncul ketika menggenerasi berkas PDF:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="447"/>
        <source>Save PDF file</source>
        <translation>Simpan berkas PDF</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>Profil baru…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="32"/>
        <source>Delete profile</source>
        <translation>Hapus profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="37"/>
        <source>Manage multipage profiles</source>
        <translation>Kelola profil multi halaman</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="94"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="96"/>
        <source>Custom profile</source>
        <translation>Profil kustom</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Error</source>
        <translation>Eror</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <source>Profile name can not be empty.</source>
        <translation>Nama profil tidak boleh kosong.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="147"/>
        <source>Disabled</source>
        <translation>Nonaktifkan</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Profile name already exists.</source>
        <translation>Nama profil sudah ada.</translation>
    </message>
</context>
</TS>
