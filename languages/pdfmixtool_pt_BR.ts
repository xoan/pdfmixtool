<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>Sobre PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="43"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="53"/>
        <source>Version %1</source>
        <translation>Versão %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="65"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>Um programa para dividir, mesclar, girar e misturar arquivos PDF.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="66"/>
        <source>Website</source>
        <translation>Site da web</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="73"/>
        <source>About</source>
        <translation>Sobre</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="89"/>
        <source>Authors</source>
        <translation>Autores</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="91"/>
        <source>Translators</source>
        <translation>Tradutores</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="100"/>
        <source>Credits</source>
        <translation>Créditos</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="118"/>
        <source>License</source>
        <translation>Licença</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="128"/>
        <source>Submit a pull request</source>
        <translation>Enviar um pull request</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>Report a bug</source>
        <translation>Informar um erro</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="130"/>
        <source>Help translating</source>
        <translation>Ajude a traduzir</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="137"/>
        <source>Contribute</source>
        <translation>Contribuir</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="153"/>
        <source>Changelog</source>
        <translation>Alterações</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Editar perfil de múltiplas páginas</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="60"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="158"/>
        <source>Left</source>
        <translation>Esquerda</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="61"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <source>Center</source>
        <translation>Centro</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="62"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="160"/>
        <source>Right</source>
        <translation>Direita</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="162"/>
        <source>Top</source>
        <translation>Acima</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="164"/>
        <source>Bottom</source>
        <translation>Abaixo</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="97"/>
        <source>Name:</source>
        <translation>Nome:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="105"/>
        <source>Output page size</source>
        <translation>Tamanho da página de saída</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="107"/>
        <source>Standard size:</source>
        <translation>Tamanho padrão:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="110"/>
        <source>Custom size:</source>
        <translation>Tam. personalizado:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="113"/>
        <source>Width:</source>
        <translation>Largura:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="116"/>
        <source>Height:</source>
        <translation>Altura:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="124"/>
        <source>Pages layout</source>
        <translation>Layout das páginas</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="126"/>
        <source>Rows:</source>
        <translation>Linhas:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Columns:</source>
        <translation>Colunas:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rotation:</source>
        <translation>Rotação:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Spacing:</source>
        <translation>Espaçamento:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="143"/>
        <source>Pages alignment</source>
        <translation>Alinhamento das páginas</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="145"/>
        <source>Horizontal:</source>
        <translation>Horizontal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="148"/>
        <source>Vertical:</source>
        <translation>Vertical:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="156"/>
        <source>Margins</source>
        <translation>Margens</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="33"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>Editar propriedades dos arquivos PDF</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="36"/>
        <source>No rotation</source>
        <translation>Sem rotação</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="41"/>
        <source>Disabled</source>
        <translation>Desativado</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="87"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="100"/>
        <source>Multipage:</source>
        <translation>Páginas múltiplas:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="102"/>
        <source>Rotation:</source>
        <translation>Rotação:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="97"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>portrait</source>
        <translation>retrato</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="97"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>landscape</source>
        <translation>paisagem</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="143"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="201"/>
        <source>All</source>
        <translation>Todas</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/inputpdffiledelegate.cpp" line="98"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="221"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n página(s)</numerusform>
            <numerusform>%n páginas</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="145"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="203"/>
        <source>Pages:</source>
        <translation>Páginas:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="146"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="204"/>
        <source>Multipage:</source>
        <translation>Múltiplas páginas:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="149"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="207"/>
        <source>Disabled</source>
        <translation>Desativado</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="150"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="208"/>
        <source>Rotation:</source>
        <translation>Rotação:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="176"/>
        <source>Disabled</source>
        <translation>Desativado</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="189"/>
        <source>No rotation</source>
        <translation>Sem rotação</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="195"/>
        <source>Pages:</source>
        <translation>Páginas:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="197"/>
        <source>Multipage:</source>
        <translation>Múltiplas páginas:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="199"/>
        <source>Rotation:</source>
        <translation>Rotação:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="150"/>
        <source>Add PDF file</source>
        <translation>Adic. arquivo PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="151"/>
        <source>Move up</source>
        <translation>Subir</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="152"/>
        <source>Move down</source>
        <translation>Descer</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="153"/>
        <source>Remove file</source>
        <translation>Remover arquivo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="168"/>
        <source>About</source>
        <translation>Sobre</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="172"/>
        <location filename="../src/mainwindow.cpp" line="173"/>
        <source>Generate PDF</source>
        <translation>Gerar PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="110"/>
        <source>PDF Mix Tool</source>
        <translation>Ferramenta de mixagem de PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <location filename="../src/mainwindow.cpp" line="426"/>
        <source>PDF generation error</source>
        <translation>Erro na geração do PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="210"/>
        <source>Select one or more PDF files to open</source>
        <translation>Selecione um ou mais arquivos PDF para abrir</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="143"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="145"/>
        <source>Main toolbar</source>
        <translation>Barra de ferramentas principal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="164"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="167"/>
        <source>Multipage profiles…</source>
        <translation>Perfis de múltiplas páginas…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="169"/>
        <source>Exit</source>
        <translation>Sair</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="212"/>
        <location filename="../src/mainwindow.cpp" line="449"/>
        <source>PDF files (*.pdf)</source>
        <translation>Arquivos PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="349"/>
        <source>Output pages: %1</source>
        <translation>Páginas de saída: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="359"/>
        <source>You must add at least one PDF file.</source>
        <translation>Você deve adicionar ao menos um arquivo PDF.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="382"/>
        <source>&lt;li&gt;Invalid character &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;Caractere inválido &quot;&lt;b&gt;%1&lt;/b&gt;&quot; no filtro de páginas do arquivo &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="388"/>
        <source>&lt;li&gt;Invalid interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;Intervalo inválido &quot;&lt;b&gt;%1&lt;/b&gt;&quot; no filtro de páginas do arquivo &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="394"/>
        <source>&lt;li&gt;Boundaries of interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; are out of allowed interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;Limites do intervalo &quot;&lt;b&gt;%1&lt;/b&gt;&quot; no filtro de páginas do arquivo &quot;&lt;b&gt;%2&lt;/b&gt;&quot; estão fora do intervalo permitido&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="413"/>
        <source>&lt;li&gt;Interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; is overlapping with another interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;O intervalo &quot;&lt;b&gt;%1&lt;/b&gt;&quot; no filtro de páginas do arquivo &quot;&lt;b&gt;%2&lt;/b&gt;&quot; está sobreposto a outro intervalo&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>&lt;p&gt;The PDF generation failed due to the following errors:&lt;/p&gt;</source>
        <translation>&lt;p&gt;A geração do PDF falhou devido aos seguintes erros:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="431"/>
        <source>&lt;p&gt;The following problems were encountered while generating the PDF file:&lt;/p&gt;</source>
        <translation>&lt;p&gt;Os seguintes problemas foram encontrados durante a geração do arquivo PDF:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="447"/>
        <source>Save PDF file</source>
        <translation>Salvar arquivo PDF</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>Novo perfil…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="32"/>
        <source>Delete profile</source>
        <translation>Apagar perfil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="37"/>
        <source>Manage multipage profiles</source>
        <translation>Gerenciar perfis de múltiplas páginas</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="94"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="96"/>
        <source>Custom profile</source>
        <translation>Perfil personalizado</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <source>Profile name can not be empty.</source>
        <translation>O nome do perfil não pode estar vazio.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="147"/>
        <source>Disabled</source>
        <translation>Desativado</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Profile name already exists.</source>
        <translation>O nome do perfil já existe.</translation>
    </message>
</context>
</TS>
