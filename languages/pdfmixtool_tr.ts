<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>PDF Mix Tool Hakkında</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="43"/>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="53"/>
        <source>Version %1</source>
        <translation>Sürüm %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="65"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>PDF dosyalarını bölme, birleştirme, döndürme ve karıştırma yapan bir uygulama.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="66"/>
        <source>Website</source>
        <translation>Web sitesi</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="73"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="89"/>
        <source>Authors</source>
        <translation>Yazarlar</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="91"/>
        <source>Translators</source>
        <translation>Çevirmenler</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="100"/>
        <source>Credits</source>
        <translation>Emeği geçenler</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="118"/>
        <source>License</source>
        <translation>Lisans</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="128"/>
        <source>Submit a pull request</source>
        <translation>Çekme isteği gönderin</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>Report a bug</source>
        <translation>Hata bildir</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="130"/>
        <source>Help translating</source>
        <translation>Çeviri yardımı</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="137"/>
        <source>Contribute</source>
        <translation>Katkıda bulun</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="153"/>
        <source>Changelog</source>
        <translation>Değişiklikler</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Çok sayfalı profili düzenle</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="60"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="158"/>
        <source>Left</source>
        <translation>Sol</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="61"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <source>Center</source>
        <translation>Merkez</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="62"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="160"/>
        <source>Right</source>
        <translation>Sağ</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="162"/>
        <source>Top</source>
        <translation>Üst</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="164"/>
        <source>Bottom</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="97"/>
        <source>Name:</source>
        <translation>Adı:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="105"/>
        <source>Output page size</source>
        <translation>Çıktı sayfa boyutu</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="107"/>
        <source>Standard size:</source>
        <translation>Standart boyut:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="110"/>
        <source>Custom size:</source>
        <translation>Özel boyut:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="113"/>
        <source>Width:</source>
        <translation>Genişlik:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="116"/>
        <source>Height:</source>
        <translation>Yükseklik:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="124"/>
        <source>Pages layout</source>
        <translation>Sayfa düzeni</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="126"/>
        <source>Rows:</source>
        <translation>Satırlar:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Columns:</source>
        <translation>Sütunlar:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rotation:</source>
        <translation>Döndürme:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Spacing:</source>
        <translation>Aralık:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="143"/>
        <source>Pages alignment</source>
        <translation>Sayfa hizalama</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="145"/>
        <source>Horizontal:</source>
        <translation>Yatay:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="148"/>
        <source>Vertical:</source>
        <translation>Dikey:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="156"/>
        <source>Margins</source>
        <translation>Kenar boşlukları</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="33"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>PDF dosyaların özelliklerini düzenle</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="36"/>
        <source>No rotation</source>
        <translation>Döndürme</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="41"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="87"/>
        <source>OK</source>
        <translation>TAMAM</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="100"/>
        <source>Multipage:</source>
        <translation>Çok sayfalı:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="102"/>
        <source>Rotation:</source>
        <translation>Döndürme:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="97"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>portrait</source>
        <translation>dikey</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="97"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>landscape</source>
        <translation>yatay</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="143"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="201"/>
        <source>All</source>
        <translation>Tüm</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/inputpdffiledelegate.cpp" line="98"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="221"/>
        <source>%n page(s)</source>
        <translation><numerusform>%n sayfa</numerusform>
        <numerusform>%n sayfa</numerusform>
        </translation></message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="145"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="203"/>
        <source>Pages:</source>
        <translation>Sayfalar:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="146"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="204"/>
        <source>Multipage:</source>
        <translation>Çoklu sayfa:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="149"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="207"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="150"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="208"/>
        <source>Rotation:</source>
        <translation>Döndürme:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="176"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="189"/>
        <source>No rotation</source>
        <translation>Döndürme</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="195"/>
        <source>Pages:</source>
        <translation>Sayfalar:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="197"/>
        <source>Multipage:</source>
        <translation>Çok sayfalı:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="199"/>
        <source>Rotation:</source>
        <translation>Döndürme:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="150"/>
        <source>Add PDF file</source>
        <translation>PDF dosyası ekle</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="151"/>
        <source>Move up</source>
        <translation>Yukarı Taşı</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="152"/>
        <source>Move down</source>
        <translation>Aşağı taşı</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="153"/>
        <source>Remove file</source>
        <translation>Dosyayı Kaldır</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="168"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="172"/>
        <location filename="../src/mainwindow.cpp" line="173"/>
        <source>Generate PDF</source>
        <translation>PDF oluştur</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="110"/>
        <source>PDF Mix Tool</source>
        <translation>PDF Karışım Aracı</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <location filename="../src/mainwindow.cpp" line="426"/>
        <source>PDF generation error</source>
        <translation>PDF oluşturma hatası</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="210"/>
        <source>Select one or more PDF files to open</source>
        <translation>Açmak için bir veya daha fazla PDF dosya seçin</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="143"/>
        <source>Edit</source>
        <translation>Düzenle</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="145"/>
        <source>Main toolbar</source>
        <translation>Ana Araç Çubuğu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="164"/>
        <source>Menu</source>
        <translation>Menü</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="167"/>
        <source>Multipage profiles…</source>
        <translation>Çok sayfalı profiller…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="169"/>
        <source>Exit</source>
        <translation>Çıkış</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="212"/>
        <location filename="../src/mainwindow.cpp" line="449"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF dosyalar (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="349"/>
        <source>Output pages: %1</source>
        <translation>Çıktı sayfaları: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="359"/>
        <source>You must add at least one PDF file.</source>
        <translation>En az bir PDF dosyası eklemelisiniz.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="382"/>
        <source>&lt;li&gt;Invalid character &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;&quot;&lt;b&gt;%2&lt;/b&gt;&quot; dosyasının sayfa filtresinde geçersiz karakter &lt;b&gt;%1&lt;/b&gt;&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="388"/>
        <source>&lt;li&gt;Invalid interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;Geçersiz aralık&lt;b&gt;%1&lt;/b&gt;&quot;dosya filtreleri sayfalarında &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="394"/>
        <source>&lt;li&gt;Boundaries of interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; are out of allowed interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;Aralığın sınırları &quot;&lt;b&gt;%1&lt;/b&gt;&quot; dosya filtreleri sayfasında &quot;&lt;b&gt;%2&lt;/b&gt;&quot; izin verilen aralığın dışında&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="413"/>
        <source>&lt;li&gt;Interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; is overlapping with another interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;Aralık &quot;&lt;b&gt;%1&lt;/b&gt;&quot; dosya filtresi sayfasında &quot;&lt;b&gt;%2&lt;/b&gt;&quot; başka bir aralıkla çakışıyor&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>&lt;p&gt;The PDF generation failed due to the following errors:&lt;/p&gt;</source>
        <translation>&lt;p&gt;PDF oluşturma aşağıdaki hatalardan dolayı başarısız oldu:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="431"/>
        <source>&lt;p&gt;The following problems were encountered while generating the PDF file:&lt;/p&gt;</source>
        <translation>&lt;p&gt;PDF dosyası oluşturulurken aşağıdaki sorunlarla karşılaşıldı:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="447"/>
        <source>Save PDF file</source>
        <translation>PDF dosyayı kaydet</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>Yeni profil…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="32"/>
        <source>Delete profile</source>
        <translation>Profili sil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="37"/>
        <source>Manage multipage profiles</source>
        <translation>Çok sayfalı profilleri yönet</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="94"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="96"/>
        <source>Custom profile</source>
        <translation>Özel profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <source>Profile name can not be empty.</source>
        <translation>Profil adı boş olamaz.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="147"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Profile name already exists.</source>
        <translation>Profil adı zaten var.</translation>
    </message>
</context>
</TS>
