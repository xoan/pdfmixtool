<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>PDF Mix Tool について</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="43"/>
        <source>Close</source>
        <translation>閉じる</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="53"/>
        <source>Version %1</source>
        <translation>バージョン %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="65"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>PDF ファイルを分割、結合、回転、ミックスするためのアプリケーションです。</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="66"/>
        <source>Website</source>
        <translation>ウェブサイト</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="73"/>
        <source>About</source>
        <translation>このアプリケーションについて</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="89"/>
        <source>Authors</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="91"/>
        <source>Translators</source>
        <translation>翻訳者</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="100"/>
        <source>Credits</source>
        <translation>クレジット</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="118"/>
        <source>License</source>
        <translation>ライセンス</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="128"/>
        <source>Submit a pull request</source>
        <translation>プルリクエストを出す</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>Report a bug</source>
        <translation>バグを報告する</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="130"/>
        <source>Help translating</source>
        <translation>翻訳を手伝う</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="137"/>
        <source>Contribute</source>
        <translation>貢献</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="153"/>
        <source>Changelog</source>
        <translation>更新履歴</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>マルチページプロファイルの編集</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="60"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="158"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="61"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <source>Center</source>
        <translation>中央</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="62"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="160"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="162"/>
        <source>Top</source>
        <translation>上</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="164"/>
        <source>Bottom</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="97"/>
        <source>Name:</source>
        <translation>名前:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="105"/>
        <source>Output page size</source>
        <translation>出力ページサイズ</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="107"/>
        <source>Standard size:</source>
        <translation>標準サイズ:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="110"/>
        <source>Custom size:</source>
        <translation>カスタムサイズ:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="113"/>
        <source>Width:</source>
        <translation>幅:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="116"/>
        <source>Height:</source>
        <translation>高さ:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="124"/>
        <source>Pages layout</source>
        <translation>ページレイアウト</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="126"/>
        <source>Rows:</source>
        <translation>行:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Columns:</source>
        <translation>列:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rotation:</source>
        <translation>回転:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Spacing:</source>
        <translation>間隔:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="143"/>
        <source>Pages alignment</source>
        <translation>ページ配置</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="145"/>
        <source>Horizontal:</source>
        <translation>横:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="148"/>
        <source>Vertical:</source>
        <translation>縦:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="156"/>
        <source>Margins</source>
        <translation>余白</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="33"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>PDF ファイルのプロパティの編集</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="36"/>
        <source>No rotation</source>
        <translation>回転しない</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="41"/>
        <source>Disabled</source>
        <translation>無効</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="87"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="100"/>
        <source>Multipage:</source>
        <translation>マルチページ:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="102"/>
        <source>Rotation:</source>
        <translation>回転:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="97"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>portrait</source>
        <translation>縦向き</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="97"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>landscape</source>
        <translation>横向き</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="143"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="201"/>
        <source>All</source>
        <translation>すべて</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/inputpdffiledelegate.cpp" line="98"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="221"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%nページ</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="145"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="203"/>
        <source>Pages:</source>
        <translation>ページ:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="146"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="204"/>
        <source>Multipage:</source>
        <translation>マルチページ:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="149"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="207"/>
        <source>Disabled</source>
        <translation>無効</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="150"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="208"/>
        <source>Rotation:</source>
        <translation>回転:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="176"/>
        <source>Disabled</source>
        <translation>無効</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="189"/>
        <source>No rotation</source>
        <translation>回転しない</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="195"/>
        <source>Pages:</source>
        <translation>ページ:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="197"/>
        <source>Multipage:</source>
        <translation>マルチページ:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="199"/>
        <source>Rotation:</source>
        <translation>回転:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="150"/>
        <source>Add PDF file</source>
        <translation>PDF を追加</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="151"/>
        <source>Move up</source>
        <translation>上に移動</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="152"/>
        <source>Move down</source>
        <translation>下に移動</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="153"/>
        <source>Remove file</source>
        <translation>ファイルを削除</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="168"/>
        <source>About</source>
        <translation>このアプリケーションについて</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="172"/>
        <location filename="../src/mainwindow.cpp" line="173"/>
        <source>Generate PDF</source>
        <translation>PDF を生成</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="110"/>
        <source>PDF Mix Tool</source>
        <translation>PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <location filename="../src/mainwindow.cpp" line="426"/>
        <source>PDF generation error</source>
        <translation>PDF 生成エラー</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="210"/>
        <source>Select one or more PDF files to open</source>
        <translation>開く PDF ファイルを1つまたは複数選択してください</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="143"/>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="145"/>
        <source>Main toolbar</source>
        <translation>メインツールバー</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="164"/>
        <source>Menu</source>
        <translation>メニュー</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="167"/>
        <source>Multipage profiles…</source>
        <translation>マルチページプロファイル…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="169"/>
        <source>Exit</source>
        <translation>終了</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="212"/>
        <location filename="../src/mainwindow.cpp" line="449"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF ファイル (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="349"/>
        <source>Output pages: %1</source>
        <translation>出力ページ: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="359"/>
        <source>You must add at least one PDF file.</source>
        <translation>少なくとも PDF ファイルを1つ追加する必要があります。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="382"/>
        <source>&lt;li&gt;Invalid character &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;不正な文字 &quot;&lt;b&gt;%1&lt;/b&gt;&quot; がファイル &quot;&lt;b&gt;%2&lt;/b&gt;&quot; のページフィルターにあります&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="388"/>
        <source>&lt;li&gt;Invalid interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;不正な間隔 &quot;&lt;b&gt;%1&lt;/b&gt;&quot; がファイル &quot;&lt;b&gt;%2&lt;/b&gt;&quot; のページフィルターにあります&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="394"/>
        <source>&lt;li&gt;Boundaries of interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; are out of allowed interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;ファイル &quot;&lt;b&gt;%2&lt;/b&gt;&quot; のページフィルターにある間隔の境界 &quot;&lt;b&gt;%1&lt;/b&gt;&quot; は許可された範囲を超えています&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="413"/>
        <source>&lt;li&gt;Interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; is overlapping with another interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;ファイル &quot;&lt;b&gt;%2&lt;/b&gt;&quot; のページフィルターにある間隔 &quot;&lt;b&gt;%1&lt;/b&gt;&quot; は他の間隔と重なっています&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>&lt;p&gt;The PDF generation failed due to the following errors:&lt;/p&gt;</source>
        <translation>&lt;p&gt;PDF の生成は次のエラーによって失敗しました:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="431"/>
        <source>&lt;p&gt;The following problems were encountered while generating the PDF file:&lt;/p&gt;</source>
        <translation>&lt;p&gt;PDF ファイルの生成中に次の問題が発生しました:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="447"/>
        <source>Save PDF file</source>
        <translation>PDF ファイルの保存</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>新しいプロファイル…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="32"/>
        <source>Delete profile</source>
        <translation>プロファイルを削除</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="37"/>
        <source>Manage multipage profiles</source>
        <translation>マルチページプロファイルの管理</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="94"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="96"/>
        <source>Custom profile</source>
        <translation>カスタムプロファイル</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <source>Profile name can not be empty.</source>
        <translation>プロファイル名は空にできません。</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="147"/>
        <source>Disabled</source>
        <translation>無効</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Profile name already exists.</source>
        <translation>プロファイル名はすでに存在します。</translation>
    </message>
</context>
</TS>
